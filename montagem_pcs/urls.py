"""montagem_pcs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from rest_framework import routers

from auth_token.api.viewsets import CustomAuthToken
from computador.api.viewsets import ComputadorViewSet, ComputadorListViewSet
from memoriaRAM.api.viewsets import MemoriaRAMViewSet
from placaMae.api.viewsets import PlacaMaeViewSet
from placaVideo.api.viewsets import PlacaVideoViewSet
from processador.api.viewsets import ProcessadorViewSet

router = routers.DefaultRouter()
router.register(r'montagem', ComputadorViewSet)
router.register(r'montagem_list', ComputadorListViewSet)
router.register(r'processador', ProcessadorViewSet)
router.register(r'placa_mae', PlacaMaeViewSet)
router.register(r'memoria_ram', MemoriaRAMViewSet)
router.register(r'placa_video', PlacaVideoViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    path('api-token-auth/', CustomAuthToken.as_view())
]
