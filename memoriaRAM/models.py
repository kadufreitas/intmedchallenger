from django.db import models


class MemoriaRAM(models.Model):
    product = models.CharField(max_length=150,default='')
    size = models.IntegerField(default=4)

    def __str__(self):
        return self.product + ' - ' + str(self.size) + 'GB'

    def natural_key(self):
        return (self.product)

