from rest_framework.viewsets import ModelViewSet

from memoriaRAM.models import MemoriaRAM
from .serializers import MemoriaRAMSerializer


class MemoriaRAMViewSet(ModelViewSet):
    queryset = MemoriaRAM.objects.all()
    serializer_class = MemoriaRAMSerializer
