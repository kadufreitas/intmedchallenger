from rest_framework.serializers import ModelSerializer

from memoriaRAM.models import MemoriaRAM


class MemoriaRAMSerializer(ModelSerializer):
    class Meta:
        model = MemoriaRAM
        fields = [
            'id',
            'product',
            'size'
        ]