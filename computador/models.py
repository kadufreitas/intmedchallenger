from django.db import models

from memoriaRAM.models import MemoriaRAM
from placaMae.models import PlacaMae
from placaVideo.models import PlacaVideo
from processador.models import Processador
from django.contrib.auth.models import User


class Computador(models.Model):
    user_id = models.CharField(max_length=150, default='')
    client_name = models.CharField(max_length=150, default='')
    processor = models.ForeignKey(Processador, on_delete=models.PROTECT)
    motherBoard = models.ForeignKey(PlacaMae, on_delete=models.PROTECT)
    memoryRAM = models.ManyToManyField(MemoriaRAM, blank=True, default=None)
    videoBoard = models.ForeignKey(PlacaVideo, on_delete=models.PROTECT, null=True, blank=True, default=None)

    def __str__(self):
        return self.client_name

