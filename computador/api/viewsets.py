from django.http import JsonResponse
from rest_framework.filters import SearchFilter
from django.core import serializers
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from computador.models import Computador
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from .serializers import ComputadorSerializer, ComputadorListSerializer


class ComputadorViewSet(ModelViewSet):
    queryset = Computador.objects.all()
    serializer_class = ComputadorSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = [SearchFilter]
    search_fields = ['user_id', 'client_name', 'processor__product',
                     'motherBoard__product', 'memoryRAM__product', 'videoBoard__product']

    def create(self, request, *args, **kwargs):
        user = request._user
        dataRequest = request.data.copy()
        dataRequest['user_id'] = str(user.id)
        dataRequest['client_name'] = user.username
        serializer = self.get_serializer(data=dataRequest)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        mother_board = data['motherBoard']
        processor = data['processor']
        memory_rams = data['memoryRAM']
        errors_list = []

        if memory_rams.__len__() == 0:
            errors_list.append('O computador precisa ter no minimo uma memória.')

        if memory_rams.__len__() > mother_board.slotsRAM:
            errors_list.append('A placa mãe escolhida não suporta o número de memorias selecionado.')

        if self.sum_memorys(memory_rams) > mother_board.totalSupportedMemoryRAM:
            errors_list.append('A placa mãe escolhida não suporta o total de memória RAM selecionado')

        if mother_board.supportedProcessors.find(processor.brand) == -1:
            errors_list.append('Esta placa mãe não aceita o tipo de processador escolhido.')

        if not mother_board.integratedVideo and not data.get('videoBoard', None):
            errors_list.append('Esta placa mãe não possui video integrado, escolha uma placa de video.')

        if errors_list.__len__() != 0:
            return self.reponse_error(errors_list)

        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        serializer.save()

    def reponse_error(self, errors_list):
        return Response({
            'errors': errors_list
        }, status=status.HTTP_406_NOT_ACCEPTABLE)

    def sum_memorys(self, memoirs):
        acc = 0
        for element in memoirs:
            acc += element.size
        return acc

class ComputadorListViewSet(ModelViewSet):
    queryset = Computador.objects.all()
    serializer_class = ComputadorListSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = [SearchFilter]
    search_fields = ['user_id', 'client_name', 'processor__product',
                     'motherBoard__product', 'memoryRAM__product', 'videoBoard__product']

    def list(self, request, *args, **kwargs):
        user_id = request._user.id
        get_queryset = Computador.objects.filter(user_id=user_id)
        queryset = self.filter_queryset(get_queryset)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
