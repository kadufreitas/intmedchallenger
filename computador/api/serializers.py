from rest_framework.serializers import ModelSerializer, RelatedField, SerializerMethodField, ListField
from computador.models import Computador
from memoriaRAM.api.serializers import MemoriaRAMSerializer
from placaMae.api.serializers import PlacaMaeSerializer
from placaVideo.api.serializers import PlacaVideoSerializer
from processador.api.serializers import ProcessadorSerializer


class ComputadorListSerializer(ModelSerializer):
    motherBoard = PlacaMaeSerializer()
    memoryRAM = MemoriaRAMSerializer(many=True)
    videoBoard = PlacaVideoSerializer()
    processor = ProcessadorSerializer()
    class Meta:
        model = Computador
        fields = [
            'id',
            'user_id',
            'client_name',
            'processor',
            'motherBoard',
            'memoryRAM',
            'videoBoard'
        ]

class ComputadorSerializer(ModelSerializer):
    class Meta:
        model = Computador
        fields = [
            'id',
            'user_id',
            'client_name',
            'processor',
            'motherBoard',
            'memoryRAM',
            'videoBoard'
        ]
