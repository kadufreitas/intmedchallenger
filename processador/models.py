from django.db import models


class Processador(models.Model):
    product = models.CharField(max_length=150)
    brand = models.CharField(max_length=150)

    def __str__(self):
        return self.product
