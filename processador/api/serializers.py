from rest_framework.serializers import ModelSerializer

from processador.models import Processador


class ProcessadorSerializer(ModelSerializer):
    class Meta:
        model = Processador
        fields = [
            'id',
            'product',
            'brand'
        ]