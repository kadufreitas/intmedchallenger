from rest_framework.viewsets import ModelViewSet

from processador.models import Processador
from .serializers import ProcessadorSerializer


class ProcessadorViewSet(ModelViewSet):
    queryset = Processador.objects.all()
    serializer_class = ProcessadorSerializer
