## intmedchallenger

#### Rotas:

```sh
$ api/montagem
```
Cria um novo pedido de montagem passando o objeto via `POST`

```sh
$ api/montagem_list
```
Retorna a lista de pedidos de montagem para o cliente logado via `GET`

```sh
$ api/login
```
Permite fazer o login e registro no app 
