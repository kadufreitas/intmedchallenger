from rest_framework.viewsets import ModelViewSet

from .serializers import PlacaMaeSerializer
from placaMae.models import PlacaMae


class PlacaMaeViewSet(ModelViewSet):
    queryset = PlacaMae.objects.all()
    serializer_class = PlacaMaeSerializer
