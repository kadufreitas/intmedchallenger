from rest_framework.serializers import ModelSerializer

from placaMae.models import PlacaMae


class PlacaMaeSerializer(ModelSerializer):
    class Meta:
        model = PlacaMae
        fields = [
            'id',
            'product',
            'supportedProcessors',
            'slotsRAM',
            'totalSupportedMemoryRAM',
            'integratedVideo',
        ]

