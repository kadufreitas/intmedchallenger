from django.db import models


class PlacaMae(models.Model):
    product = models.CharField(max_length=150, default='')
    supportedProcessors = models.CharField(max_length=150, default='')
    slotsRAM = models.IntegerField(default=2)
    totalSupportedMemoryRAM = models.IntegerField(default=0)
    integratedVideo = models.BooleanField(default=False)

    def __str__(self):
        return self.product