from rest_framework.serializers import ModelSerializer

from placaVideo.models import PlacaVideo


class PlacaVideoSerializer(ModelSerializer):
    class Meta:
        model = PlacaVideo
        fields = [
            'id',
            'product',
        ]