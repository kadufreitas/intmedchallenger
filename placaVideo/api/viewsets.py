from rest_framework.viewsets import ModelViewSet

from placaVideo.models import PlacaVideo
from .serializers import PlacaVideoSerializer


class PlacaVideoViewSet(ModelViewSet):
    queryset = PlacaVideo.objects.all()
    serializer_class = PlacaVideoSerializer
